//
//  ViewController.swift
//  PopOver
//
//  Created by iOS 20 on 17/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var myMap: MKMapView!
    @IBOutlet weak var mapTypeSC: UISegmentedControl!
    
    enum MapType:Int {
        case Standard = 0
        case Hybrid
        case Satelite
        
    }
    var city:String?
    var address:[String]?
    var zipCode:String?
    var state:String?
    var addressDic = [:]
    var addJoined:String = ""
    var myPlaceMarck:CLPlacemark?
    var actuallocation:CLLocation?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      /*  let direccion = "Bulevar Louise Pasteur, 35, 29071 Málaga, Málaga, Spain"
        let geocoder = CLGeocoder()
       
        
        geocoder.geocodeAddressString(direccion, completionHandler: {(placemarks: [CLPlacemark]?, error:NSError?) -> Void in
        
            if let placemark = placemarks?[0] {
                print(placemark)
                print(placemark.location)
                print(placemark.location?.coordinate.latitude)
                print(placemark.location?.coordinate.longitude)
                print(placemark.location?.altitude)
                
                
                let location = CLLocation(latitude: (placemark.location?.coordinate.latitude)!, longitude: (placemark.location?.coordinate.longitude)!)
                self.centerMapInLocation(location)
               
                
            }
        })
        */
        let longitude:CLLocationDegrees = -4.426089
        let latitude:CLLocationDegrees = 36.733573
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
       // actuallocation = location
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks,error) -> Void in
            
            if error != nil {
                  print(error?.localizedDescription)
                return
            }
            if placemarks!.count > 0 {
            let pm = placemarks![0]
                print(pm)
                self.myPlaceMarck = pm
                self.addressDic = (self.myPlaceMarck?.addressDictionary)!
                print(self.addressDic)
                self.city = self.addressDic["City"] as? String
                print(self.city!)
                
                self.address = self.addressDic["FormattedAddressLines"] as? [String]
                self.addJoined = self.address!.joinWithSeparator(", ")
                
                print(self.address!)
                print(self.addJoined)
                
                let anotation = MKPointAnnotation()
                anotation.coordinate = location.coordinate
                anotation.title = self.addJoined
                anotation.subtitle = "Mi localización"
              
                self.myMap.addAnnotation(anotation)
               // self.myMap.selectAnnotation(anotation, animated: true)
                
                
                let circle = MKCircle(centerCoordinate: location.coordinate, radius: 20)
        self.myMap.addOverlay(circle)
        self.centerMapInLocation(location)
                
            }
         
        })
        
        
        
    }
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.fillColor = UIColor.blueColor().colorWithAlphaComponent(0.1)
        circleRenderer.strokeColor = UIColor.whiteColor()
        circleRenderer.lineWidth = 2
        return circleRenderer
    }
    
    
    // Cambiar anotation
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseIdentifier = "pin"
        
        var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseIdentifier)
        
        if pin == nil {
            pin = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            //pin!.tintColor = UIColor.blueColor()
            
            pin!.backgroundColor = UIColor.clearColor()
            pin?.canShowCallout = true
            pin?.rightCalloutAccessoryView = UIButton(type: .InfoDark)
            pin?.image = UIImage(named: "locator")
        } else {
            pin?.annotation = annotation
        }
        return pin
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            print("HELLO")
        }
    }
    
    
    func centerMapInLocation (location:CLLocation) {
        
        let regionRadius: CLLocationDistance = 300
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        myMap.setRegion(coordinateRegion, animated: true)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func mapTypeChanged(sender: AnyObject) {
    
        let mapType = MapType(rawValue: mapTypeSC.selectedSegmentIndex)
        switch mapType! {
        case .Standard:
            myMap.mapType = MKMapType.Standard
        case .Hybrid:
            myMap.mapType = MKMapType.Hybrid
        case .Satelite:
            myMap.mapType = MKMapType.Satellite
        }
    
    }
    
    
}

